use kurbo::Affine;
use std::clone::Clone;
use std::sync::Arc;

use crate::{
    renderers::Renderer,
    view::{Trace, TraceMeta, View},
};

pub type PointXYZ = (f64, f64, f64);

#[derive(Debug, Clone, Default)]
pub struct Viewer3d {
    infos: Vec<Trace<PointXYZ>>,
}

impl Viewer3d {
    pub fn new() -> Self {
        Viewer3d { infos: Vec::new() }
    }
    pub fn into_box(self) -> Box<dyn View> {
        Box::new(self)
    }
    pub fn add_vec(&mut self, trace_vec: Vec<PointXYZ>) {
        let meta_trace = TraceMeta::new();

        let mut new_x = Vec::new();
        let mut new_y = Vec::new();
        for trace in &trace_vec {
            new_x.push(trace.0);
            new_y.push(trace.1);
        }
        //self.x_axis.re_range(&new_x);
        //self.y_axis.re_range(&new_y);

        let trace = Trace {
            meta: meta_trace,
            data: Arc::new(trace_vec),
        };
        self.infos.push(trace);
        //self.re_range();
    }
}

impl View for Viewer3d {
    fn serialis(&self) -> u64 {
        6
    }

    // What a bad API but its trick..
    fn update_trace(&mut self, index: usize, data: Vec<f64>) -> Result<(), &'static str> {
        if index < self.infos.len() {
            let mut new_data = Vec::new();
            for i_i_i in 0..(data.len() / 3) {
                new_data.push((data[i_i_i * 3], data[i_i_i * 3 + 1], data[i_i_i * 3 + 2]));
            }
            self.infos[index].data = Arc::new(new_data);
            Ok(())
        } else {
            Err("invalid data length")
        }
    }

    fn make_boxed_trait(&self) -> Box<dyn View> {
        Box::new(self.clone())
    }
    fn render(&self, _renderer: &mut dyn Renderer, _position_transform: Affine) {}
}
