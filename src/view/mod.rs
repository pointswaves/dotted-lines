use kurbo::{Affine, Point};
use std::clone::Clone;
use std::f64::INFINITY;
use std::fmt;
use std::hash::Hash;
use std::sync::Arc;

use crate::{axis::Axis, colormap::ColorMapping, renderers::Renderer};

pub mod series2d;
pub mod series3d;
pub mod structured;

pub fn apply_affine_vec_points(points: Vec<Point>, position_transform: Affine) -> Vec<Point> {
    points
        .iter()
        .copied()
        .map(|v| position_transform * v)
        .collect()
}

/* functions to deal with the fact that nan's confuse `*thing.iter().max()` */

/// You cant generally plot Nan so we dont care about it
/// Due to rust stricted rules you cant get the max/min
/// with the normal iter max and min for floats
/// This finds the max.
pub fn max_nonnana(max_vec: &[f64]) -> f64 {
    let mut max = -INFINITY;
    for val in max_vec {
        if *val != INFINITY && val > &max {
            max = *val;
        }
    }
    max
}

/// See docs for max_nonnana execpt this finds the min.
pub fn min_nonnana(min_vec: &[f64]) -> f64 {
    let mut min = INFINITY;
    for val in min_vec {
        if *val != -INFINITY && val < &min {
            min = *val;
        }
    }
    min
}

/// All Plots must implement View so that Page can store them as boxed traits
///
/// The View trait implments eveything that page may need to do to a plot
/// eg, render it, conpaire it, clone it or update it.
///
/// It is suggested that the Plot is constructed using the plots structures functions
/// and only convierted to a boxed trait at the point is is passed to the page. The make_boxed_trait
/// function can be used for this function.
pub trait View {
    /// Serialis the View in to a unic value so that views can be conpaired
    ///
    /// Views are stored by page as boxed traits so in order for page to conpaire its
    /// self to another page is must be able to tell if its views are the same without
    /// knowing the exact type of them at compile time.
    ///
    /// How the view presets its unic ness is not constrained but the reference views hash them selves.
    fn serialis(&self) -> u64;

    /// Update the contence of Views Trace
    ///
    /// Views are expected but not required to have a set of trace like objects that contain
    /// data. This function allows these to be updated while trying to be as felxible about the
    /// contence of the trace as posible.
    ///
    /// Due to the flexablity the function may not sucsed the for the fn returns a result.
    fn update_trace(&mut self, index: usize, data: Vec<f64>) -> Result<(), &'static str>;

    /// Create a cloned box trait of self
    ///
    /// This is so that Page can clone its self.
    fn make_boxed_trait(&self) -> Box<dyn View>;

    /// Render this view with the crates rendering trait
    ///
    /// The position_transform is used by the page to tell the view how to transform points in the views
    /// coordate frame (0. to 1.) to absolute co ordanates of the final render in pixels.
    fn render(&self, renderer: &mut dyn Renderer, position_transform: Affine);

    /// Helper function to render axis
    ///
    /// This funtion will never be called directly by the page but the render function may call it
    /// to draw `standard` axis.
    fn render_axis(
        &self,
        renderer: &mut dyn Renderer,
        position_transform: Affine,
        x_axis: Box<dyn Axis>,
        y_axis: Box<dyn Axis>,
    ) {
        let points = apply_affine_vec_points(
            vec![Point { x: 0.1, y: 0.1 }, Point { x: 0.1, y: 0.9 }],
            position_transform,
        );
        renderer.line((points[0].x, points[0].y, points[1].x, points[1].y));
        for (x, name) in x_axis.ticklocations().iter().zip(x_axis.ticknames()) {
            let points = apply_affine_vec_points(
                vec![Point {
                    x: 0.1 + x * 0.8,
                    y: 0.95,
                }],
                position_transform,
            );
            renderer.text((points[0].x, points[0].y), 0.0, name.as_str())
        }

        let points = apply_affine_vec_points(
            vec![Point { x: 0.1, y: 0.9 }, Point { x: 0.9, y: 0.9 }],
            position_transform,
        );
        renderer.line((points[0].x, points[0].y, points[1].x, points[1].y));
        for (y, name) in y_axis.ticklocations().iter().zip(y_axis.ticknames()) {
            let points = apply_affine_vec_points(
                vec![Point {
                    x: 0.05,
                    y: 0.9 - y * 0.8,
                }],
                position_transform,
            );
            renderer.text((points[0].x, points[0].y), 0.0, name.as_str())
        }
    }
}

// The rest of the module implements a verity of trace like objects that varius core plots re use and
// 3rd party plots can also take advantage of but are not reqired to.

#[derive(Debug, Clone, Hash)]
pub struct TraceMeta {
    color: (u8, u8, u8),
    line: bool,
    tick: bool,
}

impl TraceMeta {
    fn new() -> Self {
        TraceMeta {
            color: (200, 200, 200),
            line: true,
            tick: true,
        }
    }
}

#[derive(Debug, Clone)]
pub struct Trace<T> {
    meta: TraceMeta,
    data: Arc<Vec<T>>,
}

impl<T> Trace<T> {
    fn get_trace_len(&self) -> usize {
        self.data.len()
    }
}

impl<T: Copy> Trace<T> {
    fn get_item(&self, index: usize) -> T {
        self.data[index]
    }
}

#[derive(Clone)]
pub struct StructureMeta {
    colour_map: Arc<Box<dyn ColorMapping>>,
    width: u32,
    hight: u32,
}

impl fmt::Debug for StructureMeta {
    // This should also say the type of the colour map, maybe we can add that as function to the ColorMapping trait
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "StructureMeta, w: {}, h: {}", self.width, self.hight)
    }
}

#[derive(Debug, Clone)]
pub struct Structured<T> {
    meta: StructureMeta,
    data: Arc<Vec<T>>,
}
