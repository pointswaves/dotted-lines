use kurbo::{Affine, Point};
use std::clone::Clone;
use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::sync::Arc;

use crate::{
    axis::continues::ContinuesAxis,
    colormap::ViridisMap,
    renderers::Renderer,
    view::{max_nonnana, StructureMeta, Structured, View},
};

pub type Value = f64;

impl Default for Structured<Value> {
    fn default() -> Self {
        Self::new()
    }
}
impl Structured<Value> {
    pub fn new() -> Structured<Value> {
        Structured {
            meta: StructureMeta {
                colour_map: Arc::new(Box::new(ViridisMap::new())),
                width: 0,
                hight: 0,
            },
            data: Arc::new(Vec::new()),
        }
    }
    pub fn from_array(array: Vec<Value>, width: u32, hight: u32) -> Self {
        Structured {
            meta: StructureMeta {
                colour_map: Arc::new(Box::new(ViridisMap::new())),
                width,
                hight,
            },
            data: Arc::new(array),
        }
    }
}

impl Hash for Structured<Value> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        //self.meta.hash(state);
        "Structured<Value>".hash(state);
        format!("{:p}", self.data).hash(state);
    }
}

#[derive(Debug, Clone, Hash)]
pub struct StructureViewer {
    infos: Structured<Value>,
    cached: Vec<u8>,
}

impl StructureViewer {
    // from has a rust meaning so this should be called something else
    pub fn from_array(array: Structured<Value>) -> Self {
        StructureViewer {
            cached: Self::cache_view(&array),
            infos: array,
        }
    }
    pub fn into_box(self) -> Box<dyn View> {
        Box::new(self)
    }

    fn cache_view(array: &Structured<Value>) -> Vec<u8> {
        let max = max_nonnana(&array.data);
        let inputs: Vec<f64> = array.data.iter().map(|v: &f64| v / max).collect();
        let mut col: Vec<u8> = vec![];
        for v in inputs {
            let full = array.meta.colour_map.get_color(v);
            col.push(full.0);
            col.push(full.1);
            col.push(full.2);
        }
        col
    }
}

impl View for StructureViewer {
    fn serialis(&self) -> u64 {
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        s.finish()
    }

    // What a bad API but its trick..
    fn update_trace(&mut self, index: usize, data: Vec<f64>) -> Result<(), &'static str> {
        if index == 0 {
            self.infos.data = Arc::new(data);
            self.cached = Self::cache_view(&self.infos);
            Ok(())
        } else {
            Err("invalid data length")
        }
    }

    fn make_boxed_trait(&self) -> Box<dyn View> {
        Box::new(self.clone())
    }

    fn render(&self, renderer: &mut dyn Renderer, position_transform: Affine) {
        let xy1 = position_transform * Point { x: 0.1, y: 0.1 };
        let xy2 = position_transform * Point { x: 0.9, y: 0.9 };

        renderer.array(
            (xy1.x, xy1.y, xy2.x - xy1.x, xy2.y - xy1.y),
            &self.cached,
            2,
            &[
                self.infos.meta.width as usize,
                self.infos.meta.hight as usize,
            ],
        );

        self.render_axis(
            renderer,
            position_transform,
            Box::new(ContinuesAxis::new()),
            Box::new(ContinuesAxis::new()),
        );
    }
}
