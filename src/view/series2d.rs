use kurbo::{Affine, Point};
use std::clone::Clone;
use std::collections::hash_map::DefaultHasher;
use std::f64::INFINITY;
use std::hash::{Hash, Hasher};
use std::sync::Arc;

use crate::{
    axis::{continues::ContinuesAxis, AxisUpdate},
    renderers::Renderer,
    view::{apply_affine_vec_points, max_nonnana, min_nonnana, Trace, TraceMeta, View},
};

pub type PointXY = (f64, f64);

impl Hash for Trace<PointXY> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        "Trace<PointXY>".hash(state);
        self.meta.hash(state);
        format!("{:p}", self.data).hash(state);
    }
}

impl Trace<PointXY> {
    fn min_max_x(&self) -> (f64, f64) {
        let xsx: Vec<f64> = self.data.iter().map(|val| val.0).collect();
        (min_nonnana(&xsx), max_nonnana(&xsx))
    }
    fn min_max_y(&self) -> (f64, f64) {
        let xsx: Vec<f64> = self.data.iter().map(|val| val.1).collect();
        (min_nonnana(&xsx), max_nonnana(&xsx))
    }
}

#[derive(Debug, Clone, Hash, Default)]
pub struct Viewer {
    infos: Vec<Trace<PointXY>>,
    x_axis: ContinuesAxis,
    y_axis: ContinuesAxis,
}

impl Viewer {
    pub fn new() -> Self {
        Viewer {
            infos: Vec::new(),
            x_axis: ContinuesAxis::new(),
            y_axis: ContinuesAxis::new(),
        }
    }
    pub fn into_box(self) -> Box<dyn View> {
        Box::new(self)
    }
    pub fn add_vec(&mut self, trace_vec: Vec<PointXY>) {
        let meta_trace = TraceMeta::new();

        let mut new_x = Vec::new();
        let mut new_y = Vec::new();
        for trace in &trace_vec {
            new_x.push(trace.0);
            new_y.push(trace.1);
        }
        self.x_axis.re_range(&new_x);
        self.y_axis.re_range(&new_y);

        let trace = Trace {
            meta: meta_trace,
            data: Arc::new(trace_vec),
        };
        self.infos.push(trace);
        self.re_range();
    }
    pub fn add_arc(&mut self, trace_vec: Arc<Vec<PointXY>>) {
        let meta_trace = TraceMeta::new();

        let trace = Trace {
            meta: meta_trace,
            data: trace_vec,
        };
        self.infos.push(trace);
        self.re_range();
    }

    fn re_range(&mut self) {
        let mut max_x: f64 = -INFINITY;
        let mut min_x: f64 = INFINITY;
        let mut max_y: f64 = -INFINITY;
        let mut min_y: f64 = INFINITY;

        for j_j_j in 0..(self.infos.len()) {
            let trace = &self.infos[j_j_j];
            let (local_min_x, local_max_x) = trace.min_max_x();
            let (local_min_y, local_max_y) = trace.min_max_y();
            println!("trace {}, {}, {}", j_j_j, local_min_y, local_max_y);
            max_x = max_x.max(local_max_x);
            min_x = min_x.min(local_min_x);
            max_y = max_y.max(local_max_y);
            min_y = min_y.min(local_min_y);
        }
        //let scale_x = 0.8 / (max_x - min_x);
        //let scale_y = 0.8 / (max_y - min_y);

        self.x_axis.re_range(&[min_x, max_x]);
        self.y_axis.re_range(&[min_y, max_y]);
    }
}

impl View for Viewer {
    fn serialis(&self) -> u64 {
        let mut s = DefaultHasher::new();
        self.hash(&mut s);
        s.finish()
    }

    // What a bad API but its trick..
    fn update_trace(&mut self, index: usize, data: Vec<f64>) -> Result<(), &'static str> {
        if index < self.infos.len() {
            let mut new_data = Vec::new();
            for i_i_i in 0..(data.len() / 2) {
                new_data.push((data[i_i_i * 2], data[i_i_i * 2 + 1]));
            }
            self.infos[index].data = Arc::new(new_data);
            self.re_range();
            Ok(())
        } else {
            Err("invalid data length")
        }
    }

    fn make_boxed_trait(&self) -> Box<dyn View> {
        Box::new(self.clone())
    }

    fn render(&self, renderer: &mut dyn Renderer, position_transform: Affine) {
        self.render_axis(
            renderer,
            position_transform,
            Box::new(self.x_axis.clone()),
            Box::new(self.y_axis.clone()),
        );

        for j_j_j in 0..(self.infos.len()) {
            let trace_len = self.infos[j_j_j].get_trace_len();
            let trace = &self.infos[j_j_j];
            for i_i_i in 0..(trace_len - 1) {
                let (a, b) = trace.get_item((i_i_i) as usize);
                let (c, d) = trace.get_item((i_i_i + 1) as usize);

                let points = apply_affine_vec_points(
                    vec![
                        Point {
                            x: 0.1 + ((a - self.x_axis.min()) * self.x_axis.coif()) * 0.8,
                            y: 0.9 - ((b - self.y_axis.min()) * self.y_axis.coif()) * 0.8,
                        },
                        Point {
                            x: 0.1 + ((c - self.x_axis.min()) * self.x_axis.coif()) * 0.8,
                            y: 0.9 - ((d - self.y_axis.min()) * self.y_axis.coif()) * 0.8,
                        },
                    ],
                    position_transform,
                );
                println!("point: {:?} , {:?}", points[0], points[1]);
                renderer.line((points[0].x, points[0].y, points[1].x, points[1].y));
            }
        }
    }
}

#[cfg(test)]
mod tests {
    /// Tests that serialis actually returns the same value for diffrent viewers that are the same
    #[test]
    fn serialis() {
        use crate::view::{
            series2d::{PointXY, Viewer},
            View,
        };
        use std::sync::Arc;

        let sample_data_one: Vec<PointXY> = vec![(1., 1.), (2., 4.), (3., 9.)];
        let arc_samlpe = Arc::new(sample_data_one);

        let mut first_view = Viewer::new();
        first_view.add_arc(arc_samlpe.clone());

        let mut second_view = Viewer::new();
        second_view.add_arc(arc_samlpe.clone());

        assert_eq!(first_view.serialis(), second_view.serialis());
    }

    #[test]
    fn update_trace() {
        use crate::view::{
            series2d::{PointXY, Viewer},
            View,
        };

        let sample_data_one: Vec<PointXY> = vec![(1., 1.), (2., 4.), (3., 9.)];
        let data_two_raw = vec![1., 1., 2., 8., 3., 27.];
        let data_three_raw = vec![1., 1., 2., 8., 3., 27.];
        let mut first_view = Viewer::new();
        first_view.add_vec(sample_data_one);

        let before = first_view.serialis();
        first_view.update_trace(0, data_two_raw).unwrap();
        let after = first_view.serialis();

        assert_ne!(before, after);

        assert_eq!(
            first_view.update_trace(1, data_three_raw),
            Err("invalid data length")
        );
    }
}
