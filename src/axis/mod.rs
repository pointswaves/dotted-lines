pub mod continues;

/// The Axis trait is used by the render axis part of the View trait
pub trait Axis {
    /// The string the View will render next to the tick
    fn ticknames(&self) -> Vec<String>;

    /// The locaction along the axis the tick will be rendered
    ///
    /// Axis coordiantes mapped from 0 to 1
    /// eg as tick in the middle would be at 0.5 and a tick a
    /// the start would be 0.
    fn ticklocations(&self) -> Vec<f64>;
}

pub trait AxisUpdate<T> {
    /// re range eg, more or less
    /// This may be quite expensive
    fn re_range(&mut self, newdata: &[T]);

    /// Map the data to were it should be ploted
    ///
    /// this should be cached as this function dose not
    /// garantee to be cheap
    ///
    /// in the same way you may store X,Y you could also
    /// cache the results of this as RX, RY
    fn map(&self, data: Vec<T>) -> Vec<f64>;
}
