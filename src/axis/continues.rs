use crate::axis::{Axis, AxisUpdate};
use crate::view::{max_nonnana, min_nonnana};
use std::f64::INFINITY;
use std::hash::{Hash, Hasher};

#[derive(Clone, Debug, Default)]
pub struct ContinuesAxis {
    tickname: Vec<String>,
    ticklocation: Vec<f64>,
    min: f64,
    coif: f64,
}

impl Hash for ContinuesAxis {
    fn hash<H: Hasher>(&self, state: &mut H) {
        "ContinuesAxis".hash(state);
        format!("{:}", self.min).hash(state);
        format!("{:}", self.coif).hash(state);
    }
}
impl ContinuesAxis {
    pub fn new() -> Self {
        ContinuesAxis {
            tickname: Vec::new(),
            ticklocation: Vec::new(),
            min: 0.,
            coif: 1.,
        }
    }
    pub fn re_name(&mut self) {
        self.ticklocation = vec![0.0, 0.5, 1.0];
        self.tickname = self
            .ticklocation
            .iter()
            .map(|number| format!("{}", self.min + number / self.coif))
            .collect();
    }
    pub fn from_axis(axisdata: &[f64]) -> Self {
        let min = min_nonnana(axisdata);
        let max = max_nonnana(axisdata);
        let coif = 1.0 / (max - min);
        let ticklocation: Vec<f64> = vec![0.0, 0.5, 1.0];
        let tickname: Vec<String> = ticklocation
            .iter()
            .map(|number| format!("{}", min + number / coif))
            .collect();
        ContinuesAxis {
            tickname,
            ticklocation,
            min,
            coif,
        }
    }
    pub fn min(&self) -> f64 {
        self.min
    }
    pub fn coif(&self) -> f64 {
        self.coif
    }
}

impl Axis for ContinuesAxis {
    /// ticknames should return the names of the ticks
    fn ticknames(&self) -> Vec<String> {
        self.tickname.clone()
    }

    /// ticknames should return the names of the ticks
    fn ticklocations(&self) -> Vec<f64> {
        self.ticklocation.clone()
    }
}
impl AxisUpdate<f64> for ContinuesAxis {
    fn re_range(&mut self, newdata: &[f64]) {
        let min = min_nonnana(newdata);
        let max = max_nonnana(newdata);
        let coif = 1.0 / (max - min);
        self.min = min;
        self.coif = coif;
        self.re_name();
    }

    fn map(&self, all_data: Vec<f64>) -> Vec<f64> {
        all_data
            .iter()
            .map(|val| {
                if *val < self.min {
                    -INFINITY
                } else {
                    let new_val = self.coif * (val - self.min);
                    if new_val > 1. {
                        INFINITY
                    } else {
                        new_val
                    }
                }
            })
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_map() {
        let bob = ContinuesAxis {
            tickname: vec![String::from("bob"), String::from("bobet")],
            ticklocation: vec![0., 1.],
            min: 0.,
            coif: 1.,
        };
        assert_eq!(
            bob.map(vec![-1., -1., 0., 0., 0.5, 1., 2., 2.]),
            vec![-INFINITY, -INFINITY, 0., 0., 0.5, 1., INFINITY, INFINITY]
        );
    }

    #[test]
    fn remap() {
        let mut bob = ContinuesAxis {
            tickname: vec![String::from("bob"), String::from("bobet")],
            ticklocation: vec![0., 1.],
            min: 0.,
            coif: 1.,
        };
        bob.re_range(&vec![5.0, 10.0]);
        assert_eq!(bob.min, 5.0);
        assert_eq!(bob.coif, 0.2);
    }

    #[test]
    fn creation() {
        let input: Vec<f64> = vec![-INFINITY, 10., 5.0, 6.0, 7.0, INFINITY];
        let ax = ContinuesAxis::from_axis(&input);

        assert_eq!(ax.ticklocations(), vec![0., 0.5, 1.]);
        assert_eq!(
            ax.ticknames(),
            vec![String::from("5"), String::from("7.5"), String::from("10")]
        );
        assert_eq!(ax.map(input), vec![-INFINITY, 1., 0., 0.2, 0.4, INFINITY]);
    }
}
