use crate::renderers::Renderer;
use crate::view::View;
use kurbo::Affine;
use std::cmp::PartialEq;

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Layout {
    x1: f64,
    x2: f64,
    y1: f64,
    y2: f64,
}

impl Layout {
    pub fn full_fig() -> Self {
        Layout {
            x1: 0.,
            x2: 1.,
            y1: 0.,
            y2: 1.,
        }
    }
    pub fn get_affine(&self, width: f64, hieght: f64) -> Affine {
        Affine::new([
            (self.x2 - self.x1) * width,
            0.,
            0.,
            (self.y2 - self.y1) * hieght,
            self.x1 * width,
            self.y1 * hieght,
        ])
    }
}

struct ViewMeta {
    layout: Layout,
    view: Box<dyn View>,
}

impl PartialEq for ViewMeta {
    fn eq(&self, other: &Self) -> bool {
        self.layout == other.layout && self.view.serialis() == other.view.serialis()
    }
}

impl Clone for ViewMeta {
    fn clone(&self) -> Self {
        ViewMeta {
            layout: self.layout,
            view: self.view.make_boxed_trait(),
        }
    }
}

/// The page structure provides the user facing api for rendering and interaciting with plots
///
/// The page allows you to arange a arbitarry number of views on a single
/// layout. The page can take any box<dyn View> and update and render it.
#[derive(Clone, PartialEq, Default)]
pub struct Page {
    views: Vec<ViewMeta>,
}

impl Page {
    pub fn new() -> Self {
        Page { views: Vec::new() }
    }

    /// Create a page from a single
    ///
    /// The view will take up the hole page
    pub fn from_view(view: Box<dyn View>) -> Self {
        Page {
            views: vec![ViewMeta {
                view: view,
                layout: Layout::full_fig(),
            }],
        }
    }

    /// Add a view to a existing page
    ///
    /// The page can be broken up in to a arbitarry number of rows and columns
    /// and then the index of row and column for the view to be shown can be specified
    pub fn add_view(
        &mut self,
        view: Box<dyn View>,
        cols: u32,
        rows: u32,
        col_index: u32,
        row_index: u32,
    ) -> Result<(), ()> {
        if (row_index >= rows) || (col_index >= cols) {
            Err(())
        } else {
            let width = 1. / f64::from(cols);
            let hieght = 1. / f64::from(rows);
            self.views.push(ViewMeta {
                view,
                layout: Layout {
                    x1: width * col_index as f64,
                    x2: width * (col_index + 1) as f64,
                    y1: hieght * row_index as f64,
                    y2: hieght * (row_index + 1) as f64,
                },
            });
            Ok(())
        }
    }

    // What a bad API but its trick..
    pub fn update_trace(
        &mut self,
        index_view: usize,
        index_trace: usize,
        data: Vec<f64>,
    ) -> Result<(), &'static str> {
        if index_view < self.views.len() {
            let mut new_data = Vec::new();
            for i_i_i in 0..(data.len() / 2) {
                new_data.push((data[i_i_i * 2], data[i_i_i * 2 + 1]));
            }
            self.views[index_view]
                .view
                .update_trace(index_trace, data)?;
            Ok(())
        } else {
            Err("invalid data length")
        }
    }

    pub fn render(&self, plot: &mut dyn Renderer, width: f64, hieght: f64) {
        for i_i_i in 0..self.views.len() {
            let render_view = &self.views[i_i_i];
            render_view
                .view
                .render(plot, render_view.layout.get_affine(width, hieght));
        }
    }

    #[cfg(feature = "images")]
    pub fn save(&self) {
        // This needs a path and then to workout what do based on extention
        // each type can then be behind a feature flag

        use crate::renderers::image::ImageRenderer;
        use piet::{Color, ImageFormat, RenderContext};
        use piet_common::Device;

        let mut device = Device::new().unwrap();
        let width = 640;
        let height = 480;
        let mut bitmap = device.bitmap_target(width, height, 1.0).unwrap();
        let mut rc = bitmap.render_context();
        rc.clear(Color::WHITE);

        let mut imren = ImageRenderer::new_raw(&mut rc);
        self.render(&mut imren, width as f64, height as f64);

        let raw_pixels = bitmap.into_raw_pixels(ImageFormat::RgbaPremul).unwrap();
        image::save_buffer(
            "temp-image.png",
            &raw_pixels,
            width as u32,
            height as u32,
            image::ColorType::RGBA(8),
        )
        .unwrap();
    }
}

#[cfg(feature = "druid")]
use crate::utils::druid_widget::{DataPage, DruidPageWidget};
#[cfg(feature = "druid")]
use druid::widget::WidgetExt;
#[cfg(feature = "druid")]
use druid::{AppLauncher, Data, Lens, Widget, WindowDesc};
#[cfg(feature = "druid")]
pub fn show(pages: Vec<Page>) {
    for page in pages {
        #[derive(Clone, Data, Lens)]
        struct AppData {
            page: DataPage,
        }
        let this_app = AppData {
            page: DataPage::new(page),
        };

        fn ui_builder() -> impl Widget<AppData> {
            DruidPageWidget::new().lens(AppData::page)
        };

        let main_window = WindowDesc::new(ui_builder);
        AppLauncher::with_window(main_window)
            .use_simple_logger()
            .launch(this_app)
            .expect("launch failed");
    }
}
