use piet::kurbo::{Line, Point, Rect};
use piet::{
    Color, FontBuilder, ImageFormat, InterpolationMode, RenderContext, Text, TextLayoutBuilder,
};
use piet_common::Piet;

use crate::renderers::{RenderArc, RenderBox, RenderLine, RenderPoint, Renderer};

pub struct ImageRenderer<'a, 'b> {
    rc: &'b mut Piet<'a>,
}

impl<'a, 'b> ImageRenderer<'a, 'b> {
    pub fn new_raw(rc: &'b mut Piet<'a>) -> ImageRenderer<'a, 'b> {
        ImageRenderer { rc }
    }
}

impl<'a, 'b> Renderer for ImageRenderer<'a, 'b> {
    fn line(&mut self, render_line: RenderLine) {
        let brush = self.rc.solid_brush(Color::rgb8(0x00, 0x00, 0x80));
        self.rc.stroke(
            Line::new(
                (render_line.0, render_line.1),
                (render_line.2, render_line.3),
            ),
            &brush,
            1.0,
        );
        self.rc.finish().unwrap();
    }
    fn arch(&mut self, _render_arc: RenderArc) {}
    fn mark(&mut self, _render_point: RenderPoint) {}
    fn array(&mut self, plotarea: RenderBox, s: &[u8], _dims: usize, dimsizes: &[usize]) {
        let im = self
            .rc
            .make_image(
                dimsizes[0] as usize,
                dimsizes[1] as usize,
                s,
                ImageFormat::Rgb,
            )
            .unwrap();

        let rec = Rect::from_origin_size((plotarea.0, plotarea.1), (plotarea.2, plotarea.3));

        self.rc
            .draw_image(&im, rec, InterpolationMode::NearestNeighbor);
    }
    fn text(&mut self, plotpoint: RenderPoint, _orientation: f64, text: &str) {
        let brush = self.rc.solid_brush(Color::rgb8(0x00, 0x80, 0x80));
        let origin = Point::new(plotpoint.0, plotpoint.1);
        println!("Paint text here {:?}", origin);

        let text_layout = self.rc.text();
        let font = text_layout
            .new_font_by_name("sans-serif", 12.0)
            .build()
            .unwrap();
        let layout = text_layout.new_text_layout(&font, text).build().unwrap();

        self.rc.draw_text(&layout, origin, &brush);
    }
}
