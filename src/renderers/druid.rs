use druid::{
    kurbo::{Line, Point, Rect},
    piet::{
        Color, FontBuilder, ImageFormat, InterpolationMode, PietText, RenderContext, Text,
        TextLayoutBuilder,
    },
    PaintCtx,
};

use crate::renderers::{RenderArc, RenderBox, RenderLine, RenderPoint, Renderer};
/// A structure to hold PaintCtx to impl the Renderer trait
///
/// The PaintCtx in side a widgets paint function already has
/// two lifetimes already so this widget must extend the life time.
pub struct PlotterPaintCtx<'a, 'b, 'c> {
    context: &'c mut PaintCtx<'a, 'b>,
}

impl<'a, 'b, 'c> PlotterPaintCtx<'a, 'b, 'c> {
    /// Create a PlotterPaintCtx from a druid PaintCtx.
    ///
    /// This structure will own the mutatable borrow for its life time
    /// there for this object can be created in side the paint function of a widget
    /// it lives till the end of the paint function but implments the Renderer trait
    /// so that this can be given to a page to render on to the widget.
    pub fn new(context_ob: &'c mut PaintCtx<'a, 'b>) -> PlotterPaintCtx<'a, 'b, 'c> {
        PlotterPaintCtx {
            context: context_ob,
        }
    }
}

/// Implement the plotlib Renderer trait
///
/// This allows PlotterPaintCtx to be a agument of a plotlib's page's plot function
impl<'a, 'b, 'c> Renderer for PlotterPaintCtx<'a, 'b, 'c> {
    fn line(&mut self, render_line: RenderLine) {
        let brush = self.context.solid_brush(Color::rgb8(0x00, 0x00, 0x80));
        self.context.stroke(
            Line::new(
                (render_line.0, render_line.1),
                (render_line.2, render_line.3),
            ),
            &brush,
            1.0,
        );
        self.context.finish().unwrap();
    }
    fn arch(&mut self, _render_arc: RenderArc) {}
    fn mark(&mut self, _render_point: RenderPoint) {}
    fn array(&mut self, plotarea: RenderBox, s: &[u8], _dims: usize, dimsizes: &[usize]) {
        let im = self
            .context
            .make_image(
                dimsizes[0] as usize,
                dimsizes[1] as usize,
                s,
                ImageFormat::Rgb,
            )
            .unwrap();

        let rec = Rect::from_origin_size((plotarea.0, plotarea.1), (plotarea.2, plotarea.3));

        self.context
            .draw_image(&im, rec, InterpolationMode::NearestNeighbor);
    }
    fn text(&mut self, plotpoint: RenderPoint, _orientation: f64, text: &str) {
        let brush = self.context.solid_brush(Color::rgb8(0x00, 0x80, 0x80));
        let origin = Point::new(plotpoint.0, plotpoint.1);
        println!("Paint text here {:?}", origin);

        let mut text_layout = PietText::new();
        let font = text_layout
            .new_font_by_name("sans-serif", 12.0)
            .build()
            .unwrap();
        let layout = text_layout.new_text_layout(&font, text).build().unwrap();

        self.context.draw_text(&layout, origin, &brush);
    }
}
