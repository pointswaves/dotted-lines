#[cfg(any(feature = "druid"))]
pub mod druid;
#[cfg(any(feature = "images"))]
pub mod image;
#[cfg(any(feature = "svg"))]
pub mod svg;

pub type RenderBox = (f64, f64, f64, f64);
pub type RenderLine = (f64, f64, f64, f64);
pub type RenderPoint = (f64, f64);
pub type RenderArc = (f64, f64, f64, f64);

pub trait Renderer {
    fn line(&mut self, render_line: RenderLine);
    fn arch(&mut self, render_arc: RenderArc);
    fn mark(&mut self, render_point: RenderPoint);
    fn array(&mut self, plotarea: RenderBox, s: &[u8], dims: usize, dimsizes: &[usize]);
    fn text(&mut self, plotpoint: RenderPoint, orientation: f64, text: &str);
}
