use crate::renderers::{RenderArc, RenderBox, RenderLine, RenderPoint, Renderer};

use std::path::Path;

use svg;
use svg::node;
use svg::Node;

#[derive(Default)]
pub struct SvgRenderer {
    top: node::element::Group,
}

impl SvgRenderer {
    pub fn new() -> SvgRenderer {
        let top_group = node::element::Group::new();

        SvgRenderer { top: top_group }
    }
    pub fn save<P>(self, path: P, width: u32, height: u32)
    where
        P: AsRef<Path>,
    {
        let document = self.into_doc(width, height);
        svg::save(path, &document).unwrap();
    }

    pub fn into_doc(self, width: u32, height: u32) -> svg::Document {
        let mut document = svg::Document::new()
            .set("viewBox", (0, 0, width, height))
            .set("xmlns:xlink", "http://www.w3.org/1999/xlink");

        let tg = self.top;
        document.append(tg);
        document
    }
}

/// Implement the plotlib Renderer trait
///
/// This allows PlotterPaintCtx to be a agument of a plotlib's page's plot function
impl Renderer for SvgRenderer {
    fn line(&mut self, render_line: RenderLine) {
        let tick_mark = node::element::Line::new()
            .set("x1", render_line.0)
            .set("y1", render_line.1)
            .set("x2", render_line.2)
            .set("y2", render_line.3)
            .set("stroke", "black")
            .set("stroke-width", 1);
        self.top.append(tick_mark);
    }
    fn arch(&mut self, _render_arc: RenderArc) {}
    fn mark(&mut self, _render_point: RenderPoint) {}
    fn array(&mut self, _plotarea: RenderBox, _s: &[u8], _dims: usize, _dimsizes: &[usize]) {}
    fn text(&mut self, _plotpoint: RenderPoint, _orientation: f64, _text: &str) {}
}
