use druid::{
    BoxConstraints, Data, Env, Event, EventCtx, LayoutCtx, Lens, LifeCycle, LifeCycleCtx, PaintCtx,
    Size, UpdateCtx, Widget,
};

use crate::page::Page;
use crate::renderers::druid::PlotterPaintCtx;

#[derive(Default)]
pub struct DruidPageWidget {}

#[derive(Clone, Lens, Data)]
pub struct DataPage {
    #[druid(same_fn = "PartialEq::eq")]
    pub page: Page,
}

impl DataPage {
    pub fn new(data_page: Page) -> Self {
        DataPage { page: data_page }
    }
    pub fn plot(&self, paint_ctx: &mut PlotterPaintCtx, width: f64, hieght: f64) {
        self.page.render(paint_ctx, width, hieght);
    }
}

impl DruidPageWidget {
    /// Plot pages of graphs
    pub fn new() -> Self {
        DruidPageWidget {}
    }
}

impl Widget<DataPage> for DruidPageWidget {
    fn event(&mut self, _ctx: &mut EventCtx, _event: &Event, _data: &mut DataPage, _env: &Env) {}

    fn lifecycle(
        &mut self,
        _ctx: &mut LifeCycleCtx,
        _event: &LifeCycle,
        _data: &DataPage,
        _env: &Env,
    ) {
    }

    fn update(&mut self, _ctx: &mut UpdateCtx, _old_data: &DataPage, _data: &DataPage, _env: &Env) {
    }

    fn layout(
        &mut self,
        _layout_ctx: &mut LayoutCtx,
        bc: &BoxConstraints,
        _data: &DataPage,
        _env: &Env,
    ) -> Size {
        bc.debug_check("DruidPageWidget");

        if bc.is_width_bounded() {
            bc.max()
        } else {
            bc.constrain(Size::new(100., 100.))
        }
    }

    fn paint(&mut self, paint_ctx: &mut PaintCtx, data: &DataPage, _env: &Env) {
        let size = paint_ctx.size();
        let mut renderthing = PlotterPaintCtx::new(paint_ctx);
        data.plot(&mut renderthing, size.width, size.height);
    }
}
