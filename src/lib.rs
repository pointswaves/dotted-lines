//! Extendable plotting libaray with trait based api.

pub mod axis;
pub mod colormap;
pub mod page;
pub mod renderers;
pub mod utils;
pub mod view;
