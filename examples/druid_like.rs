use std::sync::Arc;

use dottedlines::{
    page::Page,
    view::series2d::{PointXY, Viewer},
};

#[derive(Clone)]
pub struct DruidPageData {
    page: Arc<Page>,
}

fn event(change_page: &mut DruidPageData) {
    //Arc::get_mut(&mut ChangePage.page).unwrap().views[0].add_bobs();
    let new_page = Arc::make_mut(&mut change_page.page);
    //new_page.views.push(Box::new(Viewer::new()));
    //new_page.views[0].push(Trace::<PointXY>::new());
    new_page
        .update_trace(0, 1, vec![5., 5., 6., 7., 8., 5.])
        .unwrap();
    //new_page.views[1].change_data()
    //change_page.page = Arc::new(*new_page);
}

fn main() {
    let mut this_view = Viewer::new();
    let sample_data_one: Vec<PointXY> = vec![(1., 1.), (2., 4.), (3., 9.)];
    this_view.add_vec(sample_data_one);
    let sample_data_two: Vec<PointXY> = vec![(1., 1.), (2., 16.), (3., 81.)];
    this_view.add_vec(sample_data_two);
    let this_page = Page::from_view(this_view.into_box());

    let mut first_dp = DruidPageData {
        page: Arc::new(this_page),
    };
    {
        let second_dp = Clone::clone(&first_dp);
        println!("strong dur {:?}", Arc::strong_count(&first_dp.page));
        println!("Weak dur {:?}", Arc::strong_count(&first_dp.page));

        event(&mut first_dp);

        println!(
            "Was there a change? {:?}",
            !Arc::ptr_eq(&first_dp.page, &second_dp.page)
        );
    }
    println!("strong after {:?}", Arc::strong_count(&first_dp.page));
    println!("Weak after {:?}", Arc::strong_count(&first_dp.page));
}
