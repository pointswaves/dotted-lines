use dottedlines::{
    page::Page,
    view::series3d::{PointXYZ, Viewer3d},
};

fn main() {
    let mut this_view = Viewer3d::new();
    let sample_data_one: Vec<PointXYZ> = vec![(1., 1., 1.), (2., 4., 1.), (3., 9., 1.)];
    this_view.add_vec(sample_data_one);
    let sample_data_two: Vec<PointXYZ> = vec![(1., 1., 1.), (2., 16., 1.), (3., 81., 1.)];
    this_view.add_vec(sample_data_two);
    let mut this_page = Page::from_view(this_view.into_box());

    let maths_vec: Vec<f64> = vec![5., 5., 6., 7., 8., 5.];
    for i_i_i in 0..10 {
        let this_vec: Vec<f64> = maths_vec.iter().map(|val| val * f64::from(i_i_i)).collect();
        this_page.update_trace(0, 1, this_vec).unwrap();
    }
}
