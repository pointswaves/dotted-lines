#[cfg(not(feature = "images"))]
fn main() {
    eprintln!("This examples requires the \"images\" feature to be enabled:");
    eprintln!("cargo run --example imshow --features \"images\"");
}

#[cfg(feature = "images")]
fn main() {
    use dottedlines::{
        page::Page,
        view::{
            structured::{StructureViewer, Value},
            Structured,
        },
    };
    let w = 100;
    let h = 200;
    let mut data: Vec<f64> = Vec::new();
    for i_i_i in 0..h {
        for j_j_j in 0..w {
            data.push(i_i_i as f64 + f64::abs(j_j_j as f64 - h as f64 / 2.));
        }
    }

    let struc = Structured::<Value>::from_array(data, w, h);

    let this_view = StructureViewer::from_array(struc);
    let this_page = Page::from_view(this_view.into_box());

    this_page.save();
}
