#[cfg(feature = "druid")]
use dottedlines::{
    page::Page,
    utils::druid_widget::{DataPage, DruidPageWidget},
    view::{
        series2d::{PointXY, Viewer},
        View,
    },
};

#[cfg(feature = "druid")]
use druid::widget::{Flex, Slider, WidgetExt};
#[cfg(feature = "druid")]
use druid::{
    lens::{self, LensExt},
    AppLauncher, Data, Lens, Widget, WindowDesc,
};

#[cfg(not(feature = "druid"))]
fn main() {
    eprintln!("This examples requires the \"druid\" feature to be enabled:");
    eprintln!("cargo run --example druid_app --features \"druid\"");
}

#[cfg(feature = "druid")]
fn main() {
    let mut this_view = Viewer::new();
    let sample_data_one: Vec<PointXY> = vec![(1., 1.), (2., 4.), (3., 9.)];
    this_view.add_vec(sample_data_one);
    let sample_data_two: Vec<PointXY> = vec![(1., 1.), (2., 16.), (3., 81.)];
    this_view.add_vec(sample_data_two);

    let mut second_view = Viewer::new();

    let mut sample_data_three: Vec<PointXY> = Vec::new();
    for i_i_i in 0..20 {
        sample_data_three.push((i_i_i as f64, (i_i_i as f64).powi(2)));
    }
    second_view.add_vec(sample_data_three);

    let mut this_page = Page::new();
    this_page
        .add_view(this_view.make_boxed_trait(), 2, 1, 0, 0)
        .unwrap();
    this_page
        .add_view(second_view.make_boxed_trait(), 2, 2, 1, 0)
        .unwrap();

    {
        #[derive(Clone, Data, Lens)]
        struct AppData {
            pub page: DataPage,
            pub slider_x: f64,
            pub slider_y: f64,
        }
        let this_app = AppData {
            page: DataPage::new(this_page),
            slider_x: 0.1,
            slider_y: 0.1,
        };

        fn ui_builder() -> impl Widget<AppData> {
            let mut col = Flex::column();
            let graph = DruidPageWidget::new().lens(AppData::page);
            col.add_child(graph, 1.0);

            let slider_a = Slider::new().lens(lens::Id.map(
                // Expose shared data with children data
                |d: &AppData| (d.slider_x),
                |d: &mut AppData, x: f64| {
                    // If shared data was changed reflect the changes in our AppData
                    d.slider_x = x;
                    let mut new_val: Vec<f64> = Vec::new();
                    for i_i in 0..10 {
                        new_val.push(i_i as f64);
                        new_val.push((i_i as f64).powf(d.slider_x * 5.))
                    }
                    d.page.page.update_trace(0, 0, new_val).unwrap();
                },
            ));
            col.add_child(slider_a, 0.0);

            col
        };

        let main_window = WindowDesc::new(ui_builder);
        AppLauncher::with_window(main_window)
            .use_simple_logger()
            .launch(this_app)
            .expect("launch failed");
    }
}
