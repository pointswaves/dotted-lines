#[cfg(feature = "druid")]
use dottedlines::{
    page::{show, Page},
    view::{
        series2d::{PointXY, Viewer},
        View,
    },
};

#[cfg(not(feature = "druid"))]
fn main() {
    eprintln!("This examples requires the \"druid\" feature to be enabled:");
    eprintln!("cargo run --example druid_basic --features \"druid\"");
}

#[cfg(feature = "druid")]
fn main() {
    let mut this_view = Viewer::new();
    let sample_data_one: Vec<PointXY> = vec![(1., 1.), (2., 4.), (3., 9.)];
    this_view.add_vec(sample_data_one);
    let sample_data_two: Vec<PointXY> = vec![(1., 1.), (2., 16.), (3., 81.)];
    this_view.add_vec(sample_data_two);

    let mut second_view = Viewer::new();

    let mut sample_data_three: Vec<PointXY> = Vec::new();
    for i_i_i in 0..20 {
        sample_data_three.push((i_i_i as f64, (i_i_i as f64).powi(2)));
    }
    second_view.add_vec(sample_data_three);

    let mut this_page = Page::new();
    this_page
        .add_view(this_view.make_boxed_trait(), 2, 1, 0, 0)
        .unwrap();
    this_page
        .add_view(second_view.make_boxed_trait(), 2, 2, 1, 0)
        .unwrap();

    let mut third_view = Viewer::new();
    let sample_data_four: Vec<PointXY> = vec![(1., 9.), (2., 4.), (3., 1.)];

    third_view.add_vec(sample_data_four);
    let second_page = Page::from_view(third_view.make_boxed_trait());

    show(vec![this_page, second_page]);
}
