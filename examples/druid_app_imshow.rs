#[cfg(feature = "druid")]
use dottedlines::{
    page::Page,
    utils::druid_widget::{DataPage, DruidPageWidget},
    view::{
        structured::{StructureViewer, Value},
        Structured,
    },
};
#[cfg(feature = "druid")]
use druid::widget::{Flex, Slider, WidgetExt};
#[cfg(feature = "druid")]
use druid::{
    lens::{self, LensExt},
    AppLauncher, Data, Lens, Widget, WindowDesc,
};

#[cfg(feature = "druid")]
fn create_data(pek: f64, pck: f64) -> Vec<f64> {
    let w = 100;
    let h = 200;
    let mut data: Vec<f64> = Vec::new();
    for i_i_i in 0..h {
        for j_j_j in 0..w {
            data.push(
                f64::abs((j_j_j as f64) - pek * (w as f64))
                    + f64::abs(i_i_i as f64 - pck * (h as f64)),
            );
        }
    }
    data
}

#[cfg(not(feature = "druid"))]
fn main() {
    eprintln!("This examples requires the \"druid\" feature to be enabled:");
    eprintln!("cargo run --example druid_app_imshow --features \"druid\"");
}

#[cfg(feature = "druid")]
fn main() {
    let w = 100;
    let h = 200;

    let data = create_data(0.5, 0.5);

    let struc = Structured::<Value>::from_array(data, w, h);

    let this_view = StructureViewer::from_array(struc);
    let this_page = Page::from_view(this_view.into_box());

    {
        #[derive(Clone, Data, Lens)]
        struct AppData {
            pub page: DataPage,
            pub slider_x: f64,
            pub slider_y: f64,
        }
        let this_app = AppData {
            page: DataPage::new(this_page),
            slider_x: 0.1,
            slider_y: 0.1,
        };

        fn ui_builder() -> impl Widget<AppData> {
            let mut row = Flex::row();
            let mut col = Flex::column();
            let graph = DruidPageWidget::new().lens(AppData::page);
            col.add_child(graph, 1.0);

            let slider_a = Slider::new().lens(lens::Id.map(
                // Expose shared data with children data
                |d: &AppData| (d.slider_x),
                |d: &mut AppData, x: f64| {
                    // If shared data was changed reflect the changes in our AppData
                    d.slider_x = x;
                    let new_val = create_data(x, d.slider_y);
                    d.page.page.update_trace(0, 0, new_val).unwrap();
                },
            ));
            col.add_child(slider_a, 0.0);

            let slider_b = Slider::new().lens(lens::Id.map(
                // Expose shared data with children data
                |d: &AppData| (d.slider_y),
                |d: &mut AppData, y: f64| {
                    // If shared data was changed reflect the changes in our AppData
                    d.slider_y = y;
                    let new_val = create_data(d.slider_x, y);
                    d.page.page.update_trace(0, 0, new_val).unwrap();
                },
            ));
            row.add_child(slider_b, 0.0);

            row.add_child(col, 1.0);
            row
        };

        let main_window = WindowDesc::new(ui_builder);
        AppLauncher::with_window(main_window)
            .use_simple_logger()
            .launch(this_app)
            .expect("launch failed");
    }
}
